//
//  ContentView.swift
//  GridRules
//
//  Created by Arturo Gamarra on 5/5/21.
//

import SwiftUI

struct ContentView: View {
    
    private var simbolos = ["keyboard", "printer", "tv.fill", "headphones", "tv.music.note", "mic","video"]
    private var colors = [Color.yellow, Color.pink, Color.purple]
    
    private var layout = [GridItem(.fixed(200)),
        GridItem(.adaptive(minimum: 60, maximum: 100))]
    
    var body: some View {
        ScrollView(.horizontal) {
            LazyHGrid(rows: layout, spacing: 20, content: {
                ForEach((0..<999)) { index in
                    Image(systemName: simbolos[index % simbolos.count])
                        .font(.system(size: 30))
                        //.frame(width: 50, height: 50)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 60, maxHeight: .infinity)
                        .background(colors[index % colors.count])
                        .cornerRadius(10)
                }
            })
        }
    }
}

struct ContentVGridView: View {
    
    private var simbolos = ["keyboard", "printer", "tv.fill", "headphones", "tv.music.note", "mic","video"]
    private var colors = [Color.yellow, Color.pink, Color.purple]
    
    private var layout = [GridItem(.fixed(100)),
        GridItem(.adaptive(minimum: 60, maximum: 100))]
    
    var body: some View {
        ScrollView {
            LazyVGrid(columns: layout,spacing: 20, content: {
                
                ForEach((0..<999)) { index in
                    Image(systemName: simbolos[index % simbolos.count])
                        .font(.system(size: 30))
                        //.frame(width: 50, height: 50)
                        .frame(minWidth: 0, maxWidth: .infinity, minHeight: 60)
                        .background(colors[index % colors.count])
                        .cornerRadius(10)
                }
            })
        }
    }
}


struct ListVStack: View {
    
    private var simbolos = ["keyboard", "printer", "tv.fill", "headphones", "tv.music.note", "mic","video"]
    
    var body: some View {
        ScrollView {
            LazyVStack {
                ForEach((0..<999)) { index in
                    Image(systemName: simbolos[index % simbolos.count])
                        .font(.system(size: 30))
                        .frame(minWidth: /*@START_MENU_TOKEN@*/0/*@END_MENU_TOKEN@*/, maxWidth: /*@START_MENU_TOKEN@*/.infinity/*@END_MENU_TOKEN@*/, maxHeight: 50)
                        .padding()
                }
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView()
            ContentVGridView()
        }
    }
}
