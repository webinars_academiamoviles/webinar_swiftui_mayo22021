//
//  GridRulesApp.swift
//  GridRules
//
//  Created by Arturo Gamarra on 5/5/21.
//

import SwiftUI

@main
struct GridRulesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
